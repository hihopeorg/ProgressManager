package me.jessyan.progressmanager.demo;

import me.jessyan.progressmanager.demo.slice.AdvanceAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

public class AdvanceAbility extends FractionAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(AdvanceAbilitySlice.class.getName());
    }

}
