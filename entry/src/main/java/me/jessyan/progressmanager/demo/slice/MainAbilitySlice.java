package me.jessyan.progressmanager.demo.slice;

import me.jessyan.progressmanager.ProgressListener;
import me.jessyan.progressmanager.ProgressManager;
import me.jessyan.progressmanager.body.ProgressInfo;
import me.jessyan.progressmanager.demo.*;
import me.jessyan.progressmanager.demo.glide.LoadPictureUtils;
import me.jessyan.progressmanager.demo.okttp3.OkHttpClientUtils;
import me.jessyan.progressmanager.demo.uitls.IntentUtils;
import me.jessyan.progressmanager.demo.uitls.LogUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.ResourceManager;
import okhttp3.*;

import java.io.*;
import java.util.WeakHashMap;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private static final String TAG = "MainAbilitySlice";
    /**
     * 全局持有 url 是为了使用 {@link WeakHashMap} 的特性,在 {@link ProgressManager} 中顶部有介绍
     * 使用 String mUrl = new String("url");, 而不是 String mUrl = "url";
     * 为什么这样做? 因为如果直接使用 String mUrl = "url", 这个 url 字符串会被加入全局字符串常量池, 池中的字符串将不会被回收
     * 既然 {@code key} 没被回收, 那 {@link WeakHashMap} 中的值也不会被移除
     * 在 {@link #onStop()} 中一定记得释放被引用的 url (将 url 设为 null), 这样框架就能在 java 虚拟机 GC 时释放对应的监听器
     */
    public String mImageUrl = new String("https://raw.githubusercontent.com/JessYanCoding/MVPArmsTemplate/master/art/step.png");
    //    public String mImageUrl = new String("https://www.baidu.com/img/flexible/logo/pc/index.png");
//    public String mDownloadUrl = new String("https://raw.githubusercontent.com/JessYanCoding/MVPArmsTemplate/master/art/MVPArms.gif");
    public String mDownloadUrl = new String("https://www.baidu.com/img/flexible/logo/pc/index.png");
    public String mUploadUrl = new String("http://upload.qiniu.com/");

    private Image mImageView;
    private OkHttpClient mOkHttpClient;
    private ProgressBar mGlideProgress;
    private ProgressBar mDownloadProgress;
    private ProgressBar mUploadProgress;
    private Text mGlideProgressText;
    private Text mDownloadProgressText;
    private Text mUploadProgressText;

    private ProgressInfo mLastDownloadingInfo;
    private ProgressInfo mLastUploadingingInfo;
    private EventHandler mHandler;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
//        AbilityContext
        mOkHttpClient = OkHttpClientUtils.getOkHttpClient();
        mHandler = new EventHandler(EventRunner.getMainEventRunner());
        initView();
        initListener();

        //在 Activity 中显示进度条的同时,也在 Fragment 中显示对应 url 的进度条,为了展示此框架的多端同步更新某一个进度信息
//        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,
//                MainFragment.newInstance(mImageUrl, mDownloadUrl, mUploadUrl)).commit();

        MainAbility mainAbility = (MainAbility) getAbility();
        FractionManager fractionManager = mainAbility.getFractionManager();
        fractionManager.startFractionScheduler()
                .add(ResourceTable.Id_fragment_container, MainFraction.newInstance(mImageUrl, mDownloadUrl, mUploadUrl))
                .submit();


    }

    private void initView() {

        mImageView = (Image) findComponentById(ResourceTable.Id_imageView);
        mGlideProgress = (ProgressBar) findComponentById(ResourceTable.Id_glide_progress);
        mDownloadProgress = (ProgressBar) findComponentById(ResourceTable.Id_download_progress);
        mUploadProgress = (ProgressBar) findComponentById(ResourceTable.Id_upload_progress);
        mGlideProgressText = (Text) findComponentById(ResourceTable.Id_glide_progress_text);
        mDownloadProgressText = (Text) findComponentById(ResourceTable.Id_download_progress_text);
        mUploadProgressText = (Text) findComponentById(ResourceTable.Id_upload_progress_text);
        findComponentById(ResourceTable.Id_glide_start).setClickedListener(this);
        findComponentById(ResourceTable.Id_download_start).setClickedListener(this);
        findComponentById(ResourceTable.Id_upload_start).setClickedListener(this);
        findComponentById(ResourceTable.Id_advance).setClickedListener(this);

    }


    private void initListener() {
        //Glide 加载监听
        ProgressManager.getInstance().addResponseListener(mImageUrl, getGlideListener());


        //Okhttp/Retofit 下载监听
        ProgressManager.getInstance().addResponseListener(mDownloadUrl, getDownloadListener());


        //Okhttp/Retofit 上传监听
        ProgressManager.getInstance().addRequestListener(mUploadUrl, getUploadListener());
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


    private ProgressListener getUploadListener() {
        return new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                // 如果你不屏蔽用户重复点击上传或下载按钮,就可能存在同一个 Url 地址,上一次的上传或下载操作都还没结束,
                // 又开始了新的上传或下载操作,那现在就需要用到 id(请求开始时的时间) 来区分正在执行的进度信息
                // 这里我就取最新的上传进度用来展示,顺便展示下 id 的用法

                if (mLastUploadingingInfo == null) {
                    mLastUploadingingInfo = progressInfo;
                }

                //因为是以请求开始时的时间作为 Id ,所以值越大,说明该请求越新
                if (progressInfo.getId() < mLastUploadingingInfo.getId()) {
                    return;
                } else if (progressInfo.getId() > mLastUploadingingInfo.getId()) {
                    mLastUploadingingInfo = progressInfo;
                }

                int progress = mLastUploadingingInfo.getPercent();
                mUploadProgress.setProgressValue(progress);
                mUploadProgressText.setText(progress + "%");
                LogUtils.log(LogUtils.DEBUG, TAG, "--Upload-- " + progress + " %  " + mLastUploadingingInfo.getSpeed() + " byte/s  " + mLastUploadingingInfo.toString());
                if (mLastUploadingingInfo.isFinish()) {
                    //说明已经上传完成
                    LogUtils.log(LogUtils.DEBUG, TAG, "--Upload-- finish");
                }
            }

            @Override
            public void onError(long id, Exception e) {
                mHandler.postSyncTask(new Runnable() {
                    @Override
                    public void run() {
                        mUploadProgress.setProgressValue(0);
                        mUploadProgressText.setText("error");
                    }
                });
            }
        };
    }

    private ProgressListener getDownloadListener() {
        return new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                // 如果你不屏蔽用户重复点击上传或下载按钮,就可能存在同一个 Url 地址,上一次的上传或下载操作都还没结束,
                // 又开始了新的上传或下载操作,那现在就需要用到 id(请求开始时的时间) 来区分正在执行的进度信息
                // 这里我就取最新的下载进度用来展示,顺便展示下 id 的用法

                if (mLastDownloadingInfo == null) {
                    mLastDownloadingInfo = progressInfo;
                }

                //因为是以请求开始时的时间作为 Id ,所以值越大,说明该请求越新
                if (progressInfo.getId() < mLastDownloadingInfo.getId()) {
                    return;
                } else if (progressInfo.getId() > mLastDownloadingInfo.getId()) {
                    mLastDownloadingInfo = progressInfo;
                }

                int progress = mLastDownloadingInfo.getPercent();
                mDownloadProgress.setProgressValue(progress);
                mDownloadProgressText.setText(progress + "%");
                LogUtils.log(LogUtils.DEBUG, TAG, "--Download-- " + progress + " %  " + mLastDownloadingInfo.getSpeed() + " byte/s  " + mLastDownloadingInfo.toString());
                if (mLastDownloadingInfo.isFinish()) {
                    //说明已经下载完成
                    LogUtils.log(LogUtils.DEBUG, TAG, "--Download-- finish");
                }
            }

            @Override
            public void onError(long id, Exception e) {
                mHandler.postSyncTask(new Runnable() {
                    @Override
                    public void run() {
                        mDownloadProgress.setProgressValue(0);
                        mDownloadProgressText.setText("error");
                    }
                });
            }
        };
    }

    private ProgressListener getGlideListener() {
        return new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                int progress = progressInfo.getPercent();
                mGlideProgress.setProgressValue(progress);
                mGlideProgressText.setText(progress + "%");
                LogUtils.log(LogUtils.DEBUG, TAG, "--Glide-- " + progress + " %  " + progressInfo.getSpeed() + " byte/s  " + progressInfo.toString());
                if (progressInfo.isFinish()) {
                    //说明已经加载完成
                    LogUtils.log(LogUtils.DEBUG, TAG, "--Glide-- finish");
                }
            }

            @Override
            public void onError(long id, Exception e) {
                mHandler.postSyncTask(new Runnable() {
                    @Override
                    public void run() {
                        mGlideProgress.setProgressValue(0);
                        mGlideProgressText.setText("error");
                    }
                });
            }
        };
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_glide_start:
                glideStart();
                break;
            case ResourceTable.Id_download_start:
                downloadStart();
                break;
            case ResourceTable.Id_upload_start:
                uploadStart();
                break;
            case ResourceTable.Id_advance:
                IntentUtils.startIntent(this, AdvanceAbility.class);
                break;
        }
    }


    /**
     * 点击开始 Glide 加载图片,为了演示,就不做重复点击的处理,目前这个在支持现有的ohos 的glide中，存在一些问题，无法兼容起来，
     * 做了单独处理
     */
    private void glideStart() {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(63, 81, 181));
        LoadPictureUtils.with(this).def(shapeElement).load(mImageUrl).into(mImageView);
    }


    /**
     * 点击开始下载资源,为了演示,就不做重复点击的处理,即允许用户在还有进度没完成的情况下,使用同一个 url 开始新的下载
     */
    private void downloadStart() {
        new Thread(() -> {
            try {
                Request request = new Request.Builder()
                        .url(mDownloadUrl)
                        .build();

                Response response = mOkHttpClient.newCall(request).execute();

                InputStream is = response.body().byteStream();
                //为了方便就不动态申请权限了,直接将文件放到CacheDir()中
                File file = new File(getCacheDir(), "download");
                FileOutputStream fos = new FileOutputStream(file);
                BufferedInputStream bis = new BufferedInputStream(is);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = bis.read(buffer)) != -1) {
                    fos.write(buffer, 0, len);
                }
                fos.flush();
                fos.close();
                bis.close();
                is.close();


            } catch (IOException e) {
                e.printStackTrace();
                //当外部发生错误时,使用此方法可以通知所有监听器的 onError 方法
                ProgressManager.getInstance().notifyOnErorr(mDownloadUrl, e);
            }
        }).start();
    }

    /**
     * 点击开始上传资源,为了演示,就不做重复点击的处理,即允许用户在还有进度没完成的情况下,使用同一个 url 开始新的上传
     */
    private void uploadStart() {
        new Thread(() -> {
            try {
                //为了方便就不动态申请权限了,直接将文件放到CacheDir()中
                File file = new File(getCacheDir(), "a.java");
                //读取Assets里面的数据,作为上传源数据
                writeToFile(getRawFile(MainAbilitySlice.this, "a.java"), file);
                Request request = new Request.Builder()
                        .url(mUploadUrl)
                        .post(RequestBody.create(MediaType.parse("multipart/form-data"), file))
                        .build();

                Response response = mOkHttpClient.newCall(request).execute();
                response.body();
            } catch (IOException e) {
                e.printStackTrace();
                //当外部发生错误时,使用此方法可以通知所有监听器的 onError 方法
                ProgressManager.getInstance().notifyOnErorr(mUploadUrl, e);
            }
        }).start();
    }

    public static File writeToFile(InputStream in, File file) throws IOException {
        FileOutputStream out = new FileOutputStream(file);
        byte[] buf = new byte[1024];
        int num = 0;
        while ((num = in.read(buf)) != -1) {
            out.write(buf, 0, buf.length);
        }
        out.close();
        return file;
    }

    public static InputStream getRawFile(Context context, String fileName) throws IOException {
        ResourceManager resManager = context.getResourceManager();
        RawFileEntry rawFileEntry = resManager.getRawFileEntry("resources/rawfile/" + fileName);
        return rawFileEntry.openRawFile();
    }
}
