package me.jessyan.progressmanager.demo;

//import com.bumptech.glide.NonNull;
import me.jessyan.progressmanager.ProgressListener;
import me.jessyan.progressmanager.ProgressManager;
import me.jessyan.progressmanager.body.ProgressInfo;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.*;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;

public class AdvanceFraction extends Fraction {

    private static final String TAG = "AdvanceFragment";

    private ProgressBar mGlideProgress;
    private ProgressBar mDownloadProgress;
    private ProgressBar mUploadProgress;
    private Text mGlideProgressText;
    private Text mDownloadProgressText;
    private Text mUploadProgressText;
    private Component mRootView;

    private ProgressInfo mLastDownloadingInfo;
    private ProgressInfo mLastUploadingingInfo;
    private EventHandler mHandler;
    private static final String URL_BUNDLE_KEY = "url_bundle_key";

    private IntentParams mArguments;
    public static AdvanceFraction newInstance(String imageUrl, String downloadUrl, String uploadUrl) {
        AdvanceFraction fragment = new AdvanceFraction();
        IntentParams bundle = new IntentParams();
        ArrayList<String> list = new ArrayList<>(Arrays.asList(imageUrl, downloadUrl, uploadUrl));
        bundle.setParam(URL_BUNDLE_KEY, list);
        fragment.setArguments(bundle);
        return fragment;
    }




    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        mRootView = scatter.parse(ResourceTable.Layout_fragment_main, container, false);
        return mRootView;
    }


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        mHandler = new EventHandler(EventRunner.getMainEventRunner());
        initView();
        initData();
    }

    private void initView() {
        mGlideProgress = (ProgressBar) mRootView.findComponentById(ResourceTable.Id_glide_progress);
        mDownloadProgress = (ProgressBar) mRootView.findComponentById(ResourceTable.Id_download_progress);
        mUploadProgress = (ProgressBar) mRootView.findComponentById(ResourceTable.Id_upload_progress);
        mGlideProgressText = (Text) mRootView.findComponentById(ResourceTable.Id_glide_progress_text);
        mDownloadProgressText = (Text) mRootView.findComponentById(ResourceTable.Id_download_progress_text);
        mUploadProgressText = (Text) mRootView.findComponentById(ResourceTable.Id_upload_progress_text);


    }

    private void initData() {
        IntentParams arguments = getArguments();
        ArrayList<String> list = (ArrayList<String>) arguments.getParam(URL_BUNDLE_KEY);

        if (list == null || list.isEmpty())
            return;
        //Glide 加载监听
        ProgressManager.getInstance().addResponseListener(list.get(0), getGlideListener());

        //Okhttp/Retofit 下载监听
        ProgressManager.getInstance().addResponseListener(list.get(1), getDownloadListener());

        //Okhttp/Retofit 上传监听
        ProgressManager.getInstance().addRequestListener(list.get(2), getUploadListener());
        list.clear();//清理 list 里的引用
    }

    public void setArguments(@Nullable IntentParams args) {
//        if (mIndex >= 0 && isStateSaved()) {
//            throw new IllegalStateException("Fragment already active and state has been saved");
//        }
        mArguments = args;
    }

    final public IntentParams getArguments() {
        return mArguments;
    }

    private ProgressListener getUploadListener() {
        return new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                // 如果你不屏蔽用户重复点击上传或下载按钮,就可能存在同一个 Url 地址,上一次的上传或下载操作都还没结束,
                // 又开始了新的上传或下载操作,那现在就需要用到 id(请求开始时的时间) 来区分正在执行的进度信息
                // 这里我就取最新的上传进度用来展示,顺便展示下 id 的用法

                if (mLastUploadingingInfo == null) {
                    mLastUploadingingInfo = progressInfo;
                }

                //因为是以请求开始时的时间作为 Id ,所以值越大,说明该请求越新
                if (progressInfo.getId() < mLastUploadingingInfo.getId()) {
                    return;
                } else if (progressInfo.getId() > mLastUploadingingInfo.getId()) {
                    mLastUploadingingInfo = progressInfo;
                }


                int progress = mLastUploadingingInfo.getPercent();
                mUploadProgress.setProgressValue(progress);
                mUploadProgressText.setText(progress + "%");
            }

            @Override
            public void onError(long id, Exception e) {
                mHandler.postSyncTask(new Runnable() {
                    @Override
                    public void run() {
                        mUploadProgress.setProgressValue(0);
                        mUploadProgressText.setText("error");
                    }
                });
            }
        };
    }

    private ProgressListener getDownloadListener() {
        return new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                // 如果你不屏蔽用户重复点击上传或下载按钮,就可能存在同一个 Url 地址,上一次的上传或下载操作都还没结束,
                // 又开始了新的上传或下载操作,那现在就需要用到 id(请求开始时的时间) 来区分正在执行的进度信息
                // 这里我就取最新的下载进度用来展示,顺便展示下 id 的用法

                if (mLastDownloadingInfo == null) {
                    mLastDownloadingInfo = progressInfo;
                }

                //因为是以请求开始时的时间作为 Id ,所以值越大,说明该请求越新
                if (progressInfo.getId() < mLastDownloadingInfo.getId()) {
                    return;
                } else if (progressInfo.getId() > mLastDownloadingInfo.getId()) {
                    mLastDownloadingInfo = progressInfo;
                }

                int progress = mLastDownloadingInfo.getPercent();
                mDownloadProgress.setProgressValue(progress);
                mDownloadProgressText.setText(progress + "%");
            }

            @Override
            public void onError(long id, Exception e) {
                mHandler.postSyncTask(new Runnable() {
                    @Override
                    public void run() {
                        mDownloadProgress.setProgressValue(0);
                        mDownloadProgressText.setText("error");
                    }
                });
            }
        };
    }



    private ProgressListener getGlideListener() {
        return new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                int progress = progressInfo.getPercent();
                mGlideProgress.setProgressValue(progress);
                mGlideProgressText.setText(progress + "%");
            }

            @Override
            public void onError(long id, Exception e) {
                mHandler.postSyncTask(new Runnable() {
                    @Override
                    public void run() {
                        mGlideProgress.setProgressValue(0);
                        mGlideProgressText.setText("error");
                    }
                });
            }
        };
    }

}
