/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.progressmanager.demo.glide;


import me.jessyan.progressmanager.ProgressManager;
import me.jessyan.progressmanager.demo.okttp3.OkHttpClientUtils;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import okhttp3.Request;
import okhttp3.Response;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class LoadPictureUtils {
    /**
     * pixelmap list
     */
    private static List<PixelMap> PIXEL_MAP_LIST = new ArrayList<>();
    private static Image IMAGE;
    /**
     * animatorvalue
     */
    private static AnimatorValue ANIMATOR_VALUE;
    /**
     * abilitySlice
     */
    private Context abilitySlice;
    /**
     * url 图片地址
     */
    private String url = null;
    /**
     * default imager resources id
     */
    private int defImage;
    private Element element;
    /**
     * iamge
     */
    private Image image;
    /**
     * whole image
     */
    private PixelMap pixelMap;
    /**
     * file  type
     */
    private FileType fileType;

    /**
     * pixelmap list
     */
    private List<PixelMap> pixelMapList = new ArrayList<>();
    /**
     * animatorvalue
     */
    private AnimatorValue animatorValue;

    /**
     * init disk
     *
     * @param ability
     * @throws IOException
     */
    private LoadPictureUtils(Context ability) {
        this.abilitySlice = ability;
    }


    /**
     * init glide
     *
     * @param ability ability
     * @return OhosGlide
     * @throws IOException IOException
     */
    public static LoadPictureUtils with(Context ability) {

        return new LoadPictureUtils(ability);
    }

    /**
     * load url
     *
     * @param url url
     * @return OhosGlide
     */
    public LoadPictureUtils load(String url) {
        this.url = url;
        return this;
    }


    /**
     * 设置默认图片，当发生异常时使用
     *
     * @param defImage defImage
     * @return OhosGlide
     */
    public LoadPictureUtils def(int defImage) {
        this.defImage = defImage;
        return this;
    }

    public LoadPictureUtils def(Element element) {
        this.element = element;
        return this;
    }


    /**
     * 需要渲染的iamge
     *
     * @param image image
     */
    public void into(Image image) {
        this.image = image;

        if (url.contains(".png") || url.contains(".jpg") || url.contains(".jpeg") || url.contains(".bmp")) {
            fileType = FileType.PNG;
        } else if (url.contains(".gif")) {
            fileType = FileType.GIF;
        }
        downLoadFile();
    }


    /**
     * downLoadFile
     */
    private void downLoadFile() {
        if (image != null && defImage > 0) {
            image.setPixelMap(defImage);
        }else if(element!=null){
            image.setBackground(element);
        }

        new Thread(() -> {
            try {
                Request request = new Request.Builder()
                        .url(url)
                        .build();

                Response response = OkHttpClientUtils.getOkHttpClient().newCall(request).execute();

                byte[] bytes = response.body().bytes();
                //为了方便就不动态申请权限了,直接将文件放到CacheDir()中
                if (response.isSuccessful()) {
                    if (fileType == FileType.PNG) {
                        showImage(bytes);
                    } else if (fileType == FileType.GIF) {
                        showGif(response.body().byteStream());
                    }
                }


            } catch (IOException e) {
                e.printStackTrace();
                //当外部发生错误时,使用此方法可以通知所有监听器的 onError 方法
                if (pixelMap != null) {
                    abilitySlice.getUITaskDispatcher().asyncDispatch(() -> {
                        image.setPixelMap(pixelMap);
                    });
                } else {
                    abilitySlice.getUITaskDispatcher().asyncDispatch(() -> {
                        image.setPixelMap(defImage);
                    });
                }

                ProgressManager.getInstance().notifyOnErorr(url, e);
            }
        }).start();

    }


    /**
     * @param pixelMapList pixelMapList
     * @param image        image
     * @param i            i
     */
    public static void loadGif(List<PixelMap> pixelMapList, Image image, int i) {
        IMAGE = image;
        PIXEL_MAP_LIST = pixelMapList;
        ANIMATOR_VALUE = new AnimatorValue();
        ANIMATOR_VALUE.setCurveType(Animator.CurveType.LINEAR);
        ANIMATOR_VALUE.setDelay(100);
        ANIMATOR_VALUE.setLoopedCount(Animator.INFINITE);//无限次循环
        ANIMATOR_VALUE.setDuration(i / 3 * 100);
        ANIMATOR_VALUE.setValueUpdateListener(M_ANIMATOR_UPDATE_LISTENER);
        ANIMATOR_VALUE.start();

    }

    /**
     * 展示网络图片
     *
     * @param bytes bytes
     */
    private void showImage(byte[] bytes) {
        ImageSource imageSource = ImageSource.create(bytes, new ImageSource.SourceOptions());
        pixelMap = imageSource.createPixelmap(new ImageSource.DecodingOptions());

        /**添加缩略图
         ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
         PixelMap thumbnailPixelmap = imageSource.createThumbnailPixelmap(decodingOpts, false);
         **/

        abilitySlice.getUITaskDispatcher().asyncDispatch(() -> {

            image.setPixelMap(pixelMap);

        });
    }


    /**
     * 展示gif
     */
    private int index = 0;

    /**
     * @param inputStream inputStream
     */
    private void showGif(InputStream inputStream) {

        ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
        ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
        decodingOpts.allowPartialImage = true;
        sourceOptions.formatHint = "image/gif";
        ImageSource imageSource = ImageSource.create(inputStream, sourceOptions);

        if (imageSource != null) {
            index = 0;
            while (imageSource.createPixelmap(index, decodingOpts) != null) {
                pixelMapList.add(imageSource.createPixelmap(index, decodingOpts));
                index++;
            }
        }
        // start anim
        abilitySlice.getUITaskDispatcher().asyncDispatch(() -> {
            loadGif(pixelMapList, image, index);
        });
    }


//    /**
//     * 展示本地图片
//     *
//     * @param inputStream inputStream
//     */
//    private void setLoalImage(InputStream inputStream) {
//        ImageSource imageSource = ImageSource.create(inputStream, new ImageSource.SourceOptions());
//        PixelMap pixelMap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
//        abilitySlice.getUITaskDispatcher().asyncDispatch(() -> {
//            image.setPixelMap(pixelMap);
//        });
//    }


    /**
     * M_ANIMATOR_UPDATE_LISTENER
     */
    private static final AnimatorValue.ValueUpdateListener M_ANIMATOR_UPDATE_LISTENER
            = new AnimatorValue.ValueUpdateListener() {
        @Override
        public void onUpdate(AnimatorValue animatorValue, float v) {
            IMAGE.setPixelMap(PIXEL_MAP_LIST.get((int) (v * PIXEL_MAP_LIST.size())));
            IMAGE.invalidate();
        }
    };

}


