package me.jessyan.progressmanager.demo.slice;

import me.jessyan.progressmanager.ProgressListener;
import me.jessyan.progressmanager.ProgressManager;
import me.jessyan.progressmanager.body.ProgressInfo;
import me.jessyan.progressmanager.demo.*;
import me.jessyan.progressmanager.demo.glide.LoadPictureUtils;
import me.jessyan.progressmanager.demo.okttp3.OkHttpClientUtils;
import me.jessyan.progressmanager.demo.uitls.LogUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.Text;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import okhttp3.*;

import java.io.*;

import static me.jessyan.progressmanager.demo.slice.MainAbilitySlice.getRawFile;

public class AdvanceAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    private static final String TAG = "AdvanceActivity";
    private Image mImageView;
    private OkHttpClient mOkHttpClient;
    private ProgressBar mGlideProgress;
    private ProgressBar mDownloadProgress;
    private ProgressBar mUploadProgress;
    private Text mGlideProgressText;
    private Text mDownloadProgressText;
    private Text mUploadProgressText;

    private ProgressInfo mLastDownloadingInfo;
    private ProgressInfo mLastUploadingingInfo;
    private EventHandler mHandler;
    private String mNewImageUrl;
    private String mNewDownloadUrl;
    private String mNewUploadUrl;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_advance);

        mOkHttpClient = OkHttpClientUtils.getOkHttpClient();
        mHandler = new EventHandler(EventRunner.getMainEventRunner());
        initView();
        initListener();

        AdvanceAbility mainAbility = (AdvanceAbility) getAbility();
        FractionManager fractionManager = mainAbility.getFractionManager();
        fractionManager.startFractionScheduler()
                .add(ResourceTable.Id_fragment_container, AdvanceFraction.newInstance(mNewImageUrl, mNewDownloadUrl, mNewUploadUrl))
                .submit();

    }

    private void initView() {

        mImageView = (Image) findComponentById(ResourceTable.Id_imageView);
        mGlideProgress = (ProgressBar) findComponentById(ResourceTable.Id_glide_progress);
        mDownloadProgress = (ProgressBar) findComponentById(ResourceTable.Id_download_progress);
        mUploadProgress = (ProgressBar) findComponentById(ResourceTable.Id_upload_progress);
        mGlideProgressText = (Text) findComponentById(ResourceTable.Id_glide_progress_text);
        mDownloadProgressText = (Text) findComponentById(ResourceTable.Id_download_progress_text);
        mUploadProgressText = (Text) findComponentById(ResourceTable.Id_upload_progress_text);
        findComponentById(ResourceTable.Id_glide_start).setClickedListener(this);
        findComponentById(ResourceTable.Id_download_start).setClickedListener(this);
        findComponentById(ResourceTable.Id_upload_start).setClickedListener(this);
    }

    private void initListener() {
        //图片和下载 (上传也同样支持) 使用同一个 url 地址,是为了展示高级功能
        //高级功能是为了应对当需要使用同一个 url 地址根据 Post 请求参数的不同而下载或上传不同资源的情况
        //"http://jessyancoding.github.io/images/RxCache.png" 会重定向到 "http://jessyan.me/images/RxCache.png"
        //所以也展示了高级功能同时完美兼容重定向
        //这里需要注意的是虽然使用的是新的 url 地址进行上传或下载,但实际请求服务器的 url 地址,还是原始的 url 地址
        //在监听器内部已经进行了处理,所以高级功能并不会影响服务器的请求
        //Glide 加载监听
        mNewImageUrl = ProgressManager
                .getInstance()
                .addDiffResponseListenerOnSameUrl("http://jessyancoding.github.io/images/RxCache.png", getGlideListener());


        //Okhttp/Retofit 下载监听
        mNewDownloadUrl = ProgressManager
                .getInstance()
                .addDiffResponseListenerOnSameUrl("http://jessyancoding.github.io/images/RxCache.png", getDownloadListener());


        //Okhttp/Retofit 上传监听
        mNewUploadUrl = ProgressManager
                .getInstance()
                .addDiffRequestListenerOnSameUrl("http://upload.qiniu.com/", "test", getUploadListener());
    }

    private ProgressListener getGlideListener() {
        return new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                int progress = progressInfo.getPercent();
                mGlideProgress.setProgressValue(progress);
                mGlideProgressText.setText(progress + "%");
                LogUtils.log(LogUtils.DEBUG, TAG, "--Glide-- " + progress + " %  " + progressInfo.getSpeed() + " byte/s  " + progressInfo.toString());
                if (progressInfo.isFinish()) {
                    //说明已经加载完成
                    LogUtils.log(LogUtils.DEBUG, TAG, "--Glide-- finish");
                }
            }

            @Override
            public void onError(long id, Exception e) {
                mHandler.postTask(new Runnable() {
                    @Override
                    public void run() {
                        mGlideProgress.setProgressValue(0);
                        mGlideProgressText.setText("error");
                    }
                });
            }
        };
    }

    private ProgressListener getUploadListener() {
        return new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                // 如果你不屏蔽用户重复点击上传或下载按钮,就可能存在同一个 Url 地址,上一次的上传或下载操作都还没结束,
                // 又开始了新的上传或下载操作,那现在就需要用到 id(请求开始时的时间) 来区分正在执行的进度信息
                // 这里我就取最新的上传进度用来展示,顺便展示下 id 的用法

                if (mLastUploadingingInfo == null) {
                    mLastUploadingingInfo = progressInfo;
                }

                //因为是以请求开始时的时间作为 Id ,所以值越大,说明该请求越新
                if (progressInfo.getId() < mLastUploadingingInfo.getId()) {
                    return;
                } else if (progressInfo.getId() > mLastUploadingingInfo.getId()) {
                    mLastUploadingingInfo = progressInfo;
                }

                int progress = mLastUploadingingInfo.getPercent();
                mUploadProgress.setProgressValue(progress);
                mUploadProgressText.setText(progress + "%");
                LogUtils.log(LogUtils.DEBUG, TAG, "--Upload-- " + progress + " %  " + mLastUploadingingInfo.getSpeed() + " byte/s  " + mLastUploadingingInfo.toString());
                if (mLastUploadingingInfo.isFinish()) {
                    //说明已经上传完成
                    LogUtils.log(LogUtils.DEBUG, TAG, "--Upload-- finish");
                }
            }

            @Override
            public void onError(long id, Exception e) {
                mHandler.postTask(new Runnable() {
                    @Override
                    public void run() {
                        mUploadProgress.setProgressValue(0);
                        mUploadProgressText.setText("error");
                    }
                });
            }
        };
    }

    private ProgressListener getDownloadListener() {
        return new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                // 如果你不屏蔽用户重复点击上传或下载按钮,就可能存在同一个 Url 地址,上一次的上传或下载操作都还没结束,
                // 又开始了新的上传或下载操作,那现在就需要用到 id(请求开始时的时间) 来区分正在执行的进度信息
                // 这里我就取最新的下载进度用来展示,顺便展示下 id 的用法

                if (mLastDownloadingInfo == null) {
                    mLastDownloadingInfo = progressInfo;
                }

                //因为是以请求开始时的时间作为 Id ,所以值越大,说明该请求越新
                if (progressInfo.getId() < mLastDownloadingInfo.getId()) {
                    return;
                } else if (progressInfo.getId() > mLastDownloadingInfo.getId()) {
                    mLastDownloadingInfo = progressInfo;
                }

                int progress = mLastDownloadingInfo.getPercent();
                mDownloadProgress.setProgressValue(progress);
                mDownloadProgressText.setText(progress + "%");
                LogUtils.log(LogUtils.DEBUG, TAG, "--Download-- " + progress + " %  " + mLastDownloadingInfo.getSpeed() + " byte/s  " + mLastDownloadingInfo.toString());
                if (mLastDownloadingInfo.isFinish()) {
                    //说明已经下载完成
                    LogUtils.log(LogUtils.DEBUG, TAG, "--Download-- finish");
                }
            }

            @Override
            public void onError(long id, Exception e) {
                mHandler.postTask(new Runnable() {
                    @Override
                    public void run() {
                        mDownloadProgress.setProgressValue(0);
                        mDownloadProgressText.setText("error");
                    }
                });
            }
        };
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {

        switch (component.getId()) {
            case ResourceTable.Id_glide_start:
                glideStart();
                AdvanceAbilitySlice.this.getApplicationContext();
                break;
            case ResourceTable.Id_download_start:
                downloadStart();
                break;
            case ResourceTable.Id_upload_start:
                uploadStart();
                break;
        }

    }


    /**
     * 点击开始 Glide 加载图片,为了演示,就不做重复点击的处理,目前这个在支持现有的ohos 的glide中，存在一些问题，无法兼容起来，
     * 做了单独处理
     */
    private void glideStart() {
        LoadPictureUtils.with(this)
                .def(ResourceTable.Media_A)
                .load(mNewImageUrl)
                .into(mImageView);
    }

    /**
     * 点击开始上传资源,为了演示,就不做重复点击的处理,即允许用户在还有进度没完成的情况下,使用同一个 url 开始新的上传
     */
    private void uploadStart() {
        new Thread(() -> {
            try {
                //为了方便就不动态申请权限了,直接将文件放到CacheDir()中
                File file = new File(getCacheDir(), "a.java");
                //读取Assets里面的数据,作为上传源数据
                writeToFile(getRawFile(AdvanceAbilitySlice.this, "a.java"), file);

                Request request = new Request.Builder()
                        .url(mNewUploadUrl)
                        .post(RequestBody.create(MediaType.parse("multipart/form-data"), file))
                        .build();

                Response response = mOkHttpClient.newCall(request).execute();
                response.body();
            } catch (IOException e) {
                e.printStackTrace();
                //当外部发生错误时,使用此方法可以通知所有监听器的 onError 方法
                ProgressManager.getInstance().notifyOnErorr(mNewUploadUrl, e);
            }
        }).start();
    }

    /**
     * 点击开始下载资源,为了演示,就不做重复点击的处理,即允许用户在还有进度没完成的情况下,使用同一个 url 开始新的下载
     */
    private void downloadStart() {
        new Thread(() -> {
            try {
                Request request = new Request.Builder()
                        .url(mNewDownloadUrl)
                        .build();

                Response response = mOkHttpClient.newCall(request).execute();

                InputStream is = response.body().byteStream();
                //为了方便就不动态申请权限了,直接将文件放到CacheDir()中
                File file = new File(getCacheDir(), "download");
                FileOutputStream fos = new FileOutputStream(file);
                BufferedInputStream bis = new BufferedInputStream(is);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = bis.read(buffer)) != -1) {
                    fos.write(buffer, 0, len);
                }
                fos.flush();
                fos.close();
                bis.close();
                is.close();


            } catch (IOException e) {
                e.printStackTrace();
                //当外部发生错误时,使用此方法可以通知所有监听器的 onError 方法
                ProgressManager.getInstance().notifyOnErorr(mNewDownloadUrl, e);
            }
        }).start();
    }


    public static File writeToFile(InputStream in, File file) throws IOException {
        FileOutputStream out = new FileOutputStream(file);
        byte[] buf = new byte[1024];
        int num = 0;
        while ((num = in.read(buf)) != -1) {
            out.write(buf, 0, buf.length);
        }
        out.close();
        return file;
    }


}
