/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.progressmanager.demo.uitls;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * description logutil
 *
 * @author baihe
 * created 2021/2/8 14:52
 */
public class LogUtils {
    /**
     * info
     */
    public static final int INFO = 0;
    /**
     * error
     */
    public static final int ERROR = 1;
    /**
     * debug
     */
    public static final int DEBUG = 2;
    /**
     * warning
     */
    public static final int WARN = 3;
    public static boolean isLoggable(String var0, int var1) {
        if ("FactoryPools".equals(var0)||"DecodePath".equals(var0)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param logType LogUtils.INFO || LogUtils.ERROR || LogUtils.DEBUG|| LogUtils.WARN
     * @param tag     日志标识  根据喜好，自定义
     * @param message 需要打印的日志信息
     */
    public static void log(int logType, String tag, String message) {
        HiLogLabel lable = new HiLogLabel(HiLog.LOG_APP, 0x0, tag);
        switch (logType) {
            case INFO:
                HiLog.info(lable, message);
                break;
            case ERROR:
                HiLog.error(lable, message);
                break;
            case DEBUG:
                HiLog.debug(lable, message);
                break;
            case WARN:
                HiLog.warn(lable, message);
                break;
            default:
                break;

        }
    }


}
