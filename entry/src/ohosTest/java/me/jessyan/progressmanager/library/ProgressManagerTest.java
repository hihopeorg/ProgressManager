/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.progressmanager.library;

import me.jessyan.progressmanager.ProgressListener;
import me.jessyan.progressmanager.ProgressManager;
import me.jessyan.progressmanager.body.ProgressInfo;
import me.jessyan.progressmanager.demo.okttp3.OkHttpClientUtils;
import me.jessyan.progressmanager.demo.slice.MainAbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.ResourceManager;
import okhttp3.*;
import org.junit.Test;

import java.io.*;
import java.util.List;

import static org.junit.Assert.*;

public class ProgressManagerTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @Test
    public void addRequestListener() {
        OkHttpClientUtils.initOkHttpClient();
        OkHttpClient mOkHttpClient = OkHttpClientUtils.getOkHttpClient();
        new Thread(() -> {
            try {
                //为了方便就不动态申请权限了,直接将文件放到CacheDir()中
                File file = new File(sAbilityDelegator.getAppContext().getCacheDir(), "a.java");
                //读取Assets里面的数据,作为上传源数据
                writeToFile(getRawFile(sAbilityDelegator.getAppContext(), "a.java"), file);
                Request request = new Request.Builder()
                        .url("http://upload.qiniu.com/")
                        .post(RequestBody.create(MediaType.parse("multipart/form-data"), file))
                        .build();
                Response response = mOkHttpClient.newCall(request).execute();
                response.body();
            } catch (IOException e) {
                e.printStackTrace();
                //当外部发生错误时,使用此方法可以通知所有监听器的 onError 方法
                ProgressManager.getInstance().notifyOnErorr("http://upload.qiniu.com/", e);
            }
        }).start();
        ProgressManager.getInstance().addRequestListener("http://upload.qiniu.com/", new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                assertTrue("判断是否更新进度" + progressInfo.getPercent(), progressInfo.getPercent() > 0);

            }

            @Override
            public void onError(long id, Exception e) {
                assertTrue("判断是否更新进度" + e.toString(), e != null);
            }
        });
        sleep(5000);
    }

    @Test
    public void addResponseListener() {
        String mDownloadUrl = "https://www.baidu.com/img/flexible/logo/pc/index.png";
        OkHttpClientUtils.initOkHttpClient();
        OkHttpClient mOkHttpClient = OkHttpClientUtils.getOkHttpClient();
        new Thread(() -> {
            try {
                Request request = new Request.Builder()
                        .url(mDownloadUrl)
                        .build();
                Response response = mOkHttpClient.newCall(request).execute();
                InputStream is = response.body().byteStream();
                //为了方便就不动态申请权限了,直接将文件放到CacheDir()中
                File file = new File(sAbilityDelegator.getAppContext().getCacheDir(), "download");
                FileOutputStream fos = new FileOutputStream(file);
                BufferedInputStream bis = new BufferedInputStream(is);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = bis.read(buffer)) != -1) {
                    fos.write(buffer, 0, len);
                }
                fos.flush();
                fos.close();
                bis.close();
                is.close();


            } catch (IOException e) {
                e.printStackTrace();
                //当外部发生错误时,使用此方法可以通知所有监听器的 onError 方法
                ProgressManager.getInstance().notifyOnErorr(mDownloadUrl, e);
            }
        }).start();


        ProgressManager.getInstance().addResponseListener("https://www.baidu.com/img/flexible/logo/pc/index.png", new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                assertTrue("判断是否更新进度" + progressInfo.getPercent(), progressInfo.getPercent() > 0);
            }

            @Override
            public void onError(long id, Exception e) {
                assertTrue("判断是否更新进度" + e.toString(), e != null);
            }
        });

        sleep(5000);
    }

    @Test
    public void with() {
        OkHttpClient.Builder builder = ProgressManager.getInstance().with(new OkHttpClient.Builder());
        assertTrue("builder is not null" + builder, builder != null);
    }

    @Test
    public void wrapRequestBody() {
        OkHttpClient.Builder builder = ProgressManager.getInstance().with(new OkHttpClient.Builder());
        Interceptor mInterceptor = chain -> ProgressManager.getInstance().wrapResponseBody(chain.proceed(ProgressManager.getInstance().wrapRequestBody(chain.request())));
        builder = builder.addInterceptor(mInterceptor);
        List<Interceptor> interceptors = builder.networkInterceptors();
        assertTrue("builder is not added" + interceptors.size(), interceptors.size() > 0);

    }

    @Test
    public void wrapResponseBody() {
        OkHttpClient.Builder builder = ProgressManager.getInstance().with(new OkHttpClient.Builder());
        Interceptor mInterceptor = chain -> ProgressManager.getInstance().wrapResponseBody(chain.proceed(ProgressManager.getInstance().wrapRequestBody(chain.request())));
        builder = builder.addInterceptor(mInterceptor);
        List<Interceptor> interceptors = builder.networkInterceptors();
        assertTrue("builder is not added" + interceptors.size(), interceptors.size() > 0);
    }

    @Test
    public void addDiffResponseListenerOnSameUrl() {
        String url1 = ProgressManager.getInstance().addDiffResponseListenerOnSameUrl("http://jessyancoding.github.io/images/RxCache.png", new MyProgressListener());
        sleep(1000);
        String url2 = ProgressManager.getInstance().addDiffResponseListenerOnSameUrl("http://jessyancoding.github.io/images/RxCache.png", new MyProgressListener());
        assertTrue("url1 and url2 are equal", !url1.equals(url2));
    }


    @Test
    public void addDiffRequestListenerOnSameUrl() {
        String url1 = ProgressManager.getInstance().addDiffRequestListenerOnSameUrl("http://jessyancoding.github.io/images/RxCache.png", new MyProgressListener());
        sleep(1000);
        String url2 = ProgressManager.getInstance().addDiffRequestListenerOnSameUrl("http://jessyancoding.github.io/images/RxCache.png", new MyProgressListener());
        assertTrue("url1 and url2 are equal", !url1.equals(url2));

    }



    class MyProgressListener implements ProgressListener {


        @Override
        public void onProgress(ProgressInfo progressInfo) {

        }

        @Override
        public void onError(long id, Exception e) {

        }
    }


    public static File writeToFile(InputStream in, File file) throws IOException {
        FileOutputStream out = new FileOutputStream(file);
        byte[] buf = new byte[1024];
        int num = 0;
        while ((num = in.read(buf)) != -1) {
            out.write(buf, 0, buf.length);
        }
        out.close();
        return file;
    }

    public static InputStream getRawFile(Context context, String fileName) throws IOException {
        ResourceManager resManager = context.getResourceManager();
        RawFileEntry rawFileEntry = resManager.getRawFileEntry("resources/rawfile/" + fileName);
        return rawFileEntry.openRawFile();
    }

    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}