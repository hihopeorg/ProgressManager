/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jessyan.progressmanager.demo.slice;

import me.jessyan.progressmanager.demo.AdvanceAbility;
import me.jessyan.progressmanager.demo.MainAbility;
import me.jessyan.progressmanager.demo.ResourceTable;
import me.jessyan.progressmanager.demo.uitls.EventHelper;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Button;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.Text;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainAbilitySliceTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }

    @Test
    public void glideActivity() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(2000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Button button = (Button) mainAbilitySlice.findComponentById(ResourceTable.Id_glide_start);
        EventHelper.triggerClickEvent(mainAbility, button);
        sleep(5000);
        ProgressBar mGlideProgress = (ProgressBar) mainAbilitySlice.findComponentById(ResourceTable.Id_glide_progress);
        Text mGlideProgressText = (Text) mainAbilitySlice.findComponentById(ResourceTable.Id_glide_progress_text);
        if (mGlideProgress.getProgress() == 0) {
            assertTrue("提示网络加载错误" + mGlideProgressText.getText(), mGlideProgressText.getText().equals("error")||mGlideProgressText.getText().equals("0%"));
        } else {
            assertTrue("判断是否更新进度" + mGlideProgress.getProgress(), mGlideProgress.getProgress() > 0);
        }
        sAbilityDelegator.stopAbility(mainAbility);
    }

    @Test
    public void downloadStart() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(2000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Button button = (Button) mainAbilitySlice.findComponentById(ResourceTable.Id_download_start);
        EventHelper.triggerClickEvent(mainAbility, button);
        sleep(5000);
        ProgressBar mGlideProgress = (ProgressBar) mainAbilitySlice.findComponentById(ResourceTable.Id_download_progress);
        Text mDownloadProgressText = (Text) mainAbilitySlice.findComponentById(ResourceTable.Id_download_progress_text);
        if (mGlideProgress.getProgress() == 0) {
            assertTrue("提示网络加载错误" + mDownloadProgressText.getText(), mDownloadProgressText.getText().equals("error")||mDownloadProgressText.getText().equals("0%"));
        } else {
            assertTrue("判断是否更新进度" + mGlideProgress.getProgress(), mGlideProgress.getProgress() > 0);
        }
        sAbilityDelegator.stopAbility(mainAbility);
    }

    @Test
    public void uploadStart() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(2000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Button button = (Button) mainAbilitySlice.findComponentById(ResourceTable.Id_upload_start);
        ProgressBar mUploadProgress = (ProgressBar) mainAbilitySlice.findComponentById(ResourceTable.Id_upload_progress);
        Text mUploadProgressText = (Text) mainAbilitySlice.findComponentById(ResourceTable.Id_upload_progress_text);
        EventHelper.triggerClickEvent(mainAbility, button);
        sleep(5000);
        if (mUploadProgress.getProgress() == 0) {
            assertTrue("提示网络加载错误" + mUploadProgressText.getText(), mUploadProgressText.getText().equals("error")||mUploadProgressText.getText().equals("0%"));
        } else {
            assertTrue("判断是否更新进度" + mUploadProgress.getProgress(), mUploadProgress.getProgress() > 0);
        }
        sAbilityDelegator.stopAbility(mainAbility);
    }

    @Test
    public void advance() {
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        sleep(2000);
        MainAbilitySlice mainAbilitySlice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(mainAbility);
        Button button = (Button) mainAbilitySlice.findComponentById(ResourceTable.Id_advance);
        EventHelper.triggerClickEvent(mainAbility, button);
        sleep(3000);
        Ability advanceAbility = sAbilityDelegator.getCurrentTopAbility();
        assertTrue("判断是当前页面是否跳转"+sAbilityDelegator, advanceAbility instanceof AdvanceAbility);
        sAbilityDelegator.stopAbility(mainAbility);

    }


    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}