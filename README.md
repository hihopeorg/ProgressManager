## **ProgressManager**
本项目是基于开源项目 ProgressManager 进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/JessYanCoding/ProgressManager ）追踪到原项目版本

#### 项目介绍

* 项目名称 ：ProgressManager
* 所属系列：ohos的第三方组件适配移植
* 功能：支持Glide 图片加载，文件下载，上传等进度显示。
* 项目移植状态：完成
* 调用差异：无
* 项目作者和维护人：hihope
* 联系方式：hihope@hoperun.com
* 原项目Doc地址：https://github.com/JessYanCoding/ProgressManager
* 原项目基线版本：基线版本：1.5.0  shal:57286212882682005b73a85f943468c5eb65fa20
* 编程语言：java
* 外部库依赖：无

#### 效果展示图
![](screenshot/progressmanager.gif)

#### 安装教程

方法一：

1. 编译ProgressManager的har包progress-release.har。

2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       ……
   }
   ```
   
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法二：

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/'
     }
 }
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'me.jessyan.progressmanager.ohos:progress:1.0.0'
 }
```
#### 使用说明
（一）下载代码

(1)初始化，设置监听
```
  //Okhttp/Retofit 下载监听
        ProgressManager.getInstance().addResponseListener(mDownloadUrl, getDownloadListener());
```
(2)下载

```
  /**
     * 点击开始下载资源,为了演示,就不做重复点击的处理,即允许用户在还有进度没完成的情况下,使用同一个 url 开始新的下载
     */
    private void downloadStart() {
        new Thread(() -> {
            try {
                Request request = new Request.Builder()
                        .url(mDownloadUrl)
                        .build();
                Response response = mOkHttpClient.newCall(request).execute();
                InputStream is = response.body().byteStream();
                //为了方便就不动态申请权限了,直接将文件放到CacheDir()中
                File file = new File(getCacheDir(), "download");
                FileOutputStream fos = new FileOutputStream(file);
                BufferedInputStream bis = new BufferedInputStream(is);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = bis.read(buffer)) != -1) {
                    fos.write(buffer, 0, len);
                }
                fos.flush();
                fos.close();
                bis.close();
                is.close();


            } catch (IOException e) {
                e.printStackTrace();
                //当外部发生错误时,使用此方法可以通知所有监听器的 onError 方法
                ProgressManager.getInstance().notifyOnErorr(mDownloadUrl, e);
            }
        }).start();
    }
```
(3)下载进度变化监听

```

    private ProgressListener getDownloadListener() {
        return new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                // 如果你不屏蔽用户重复点击上传或下载按钮,就可能存在同一个 Url 地址,上一次的上传或下载操作都还没结束,
                // 又开始了新的上传或下载操作,那现在就需要用到 id(请求开始时的时间) 来区分正在执行的进度信息
                // 这里我就取最新的下载进度用来展示,顺便展示下 id 的用法

                if (mLastDownloadingInfo == null) {
                    mLastDownloadingInfo = progressInfo;
                }

                //因为是以请求开始时的时间作为 Id ,所以值越大,说明该请求越新
                if (progressInfo.getId() < mLastDownloadingInfo.getId()) {
                    return;
                } else if (progressInfo.getId() > mLastDownloadingInfo.getId()) {
                    mLastDownloadingInfo = progressInfo;
                }

                int progress = mLastDownloadingInfo.getPercent();
                mDownloadProgress.setProgressValue(progress);
                mDownloadProgressText.setText(progress + "%");
                LogUtils.log(LogUtils.DEBUG,TAG,  "--Download-- " + progress + " %  " + mLastDownloadingInfo.getSpeed() + " byte/s  " + mLastDownloadingInfo.toString());
                if (mLastDownloadingInfo.isFinish()) {
                    //说明已经下载完成
                     LogUtils.log(LogUtils.DEBUG,TAG,  "--Download-- finish");
                }
            }

            @Override
            public void onError(long id, Exception e) {
                mHandler.postSyncTask(new Runnable() {
                    @Override
                    public void run() {
                        mDownloadProgress.setProgressValue(0);
                        mDownloadProgressText.setText("error");
                    }
                });
            }
        };
    }
```
(二)上传文件

(1)上传初始化

```
    //Okhttp/Retofit 上传监听
        ProgressManager.getInstance().addRequestListener(mUploadUrl, getUploadListener());
```
(2)上传

```
    /**
     * 点击开始上传资源,为了演示,就不做重复点击的处理,即允许用户在还有进度没完成的情况下,使用同一个 url 开始新的上传
     */
    private void uploadStart() {
        new Thread(() -> {
            try {
                //为了方便就不动态申请权限了,直接将文件放到CacheDir()中
                File file = new File(getCacheDir(), "a.java");
                //读取Assets里面的数据,作为上传源数据
                writeToFile(getRawFile(MainAbilitySlice.this, "a.java"), file);
                Request request = new Request.Builder()
                        .url(mUploadUrl)
                        .post(RequestBody.create(MediaType.parse("multipart/form-data"), file))
                        .build();

                Response response = mOkHttpClient.newCall(request).execute();
                response.body();
            } catch (IOException e) {
                e.printStackTrace();
                //当外部发生错误时,使用此方法可以通知所有监听器的 onError 方法
                ProgressManager.getInstance().notifyOnErorr(mUploadUrl, e);
            }
        }).start();
    }

```
(3)上传监听变化

```

    private ProgressListener getUploadListener() {
        return new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                // 如果你不屏蔽用户重复点击上传或下载按钮,就可能存在同一个 Url 地址,上一次的上传或下载操作都还没结束,
                // 又开始了新的上传或下载操作,那现在就需要用到 id(请求开始时的时间) 来区分正在执行的进度信息
                // 这里我就取最新的上传进度用来展示,顺便展示下 id 的用法

                if (mLastUploadingingInfo == null) {
                    mLastUploadingingInfo = progressInfo;
                }

                //因为是以请求开始时的时间作为 Id ,所以值越大,说明该请求越新
                if (progressInfo.getId() < mLastUploadingingInfo.getId()) {
                    return;
                } else if (progressInfo.getId() > mLastUploadingingInfo.getId()) {
                    mLastUploadingingInfo = progressInfo;
                }

                int progress = mLastUploadingingInfo.getPercent();
                mUploadProgress.setProgressValue(progress);
                mUploadProgressText.setText(progress + "%");
                LogUtils.log(LogUtils.DEBUG, TAG, "--Upload-- " + progress + " %  " + mLastUploadingingInfo.getSpeed() + " byte/s  " + mLastUploadingingInfo.toString());
                if (mLastUploadingingInfo.isFinish()) {
                    //说明已经上传完成
                    LogUtils.log(LogUtils.DEBUG, TAG, "--Upload-- finish");
                }
            }

            @Override
            public void onError(long id, Exception e) {
                mHandler.postSyncTask(new Runnable() {
                    @Override
                    public void run() {
                        mUploadProgress.setProgressValue(0);
                        mUploadProgressText.setText("error");
                    }
                });
            }
        };
    }

```

(三)图片加载代码

(1)初始化，设置监听
```
mNewImageUrl = ProgressManager .getInstance().addDiffResponseListenerOnSameUrl("http://jessyancoding.github.io/images/RxCache.png", getGlideListener());

```
(2)加载设置
```

    /**
     * 点击开始 Glide 加载图片,为了演示,就不做重复点击的处理,目前这个在支持现有的ohos 的glide中，存在一些问题，无法兼容起来，
     * 做了单独处理
     */
    private void glideStart() {
        LoadPictureUtils.with(this)
                .def(ResourceTable.Media_A)
                .load(mNewImageUrl)
                .into(mImageView);
    }
```
(3) 图片进度加载变化监听

```

    private ProgressListener getGlideListener() {
        return new ProgressListener() {
            @Override
            public void onProgress(ProgressInfo progressInfo) {
                int progress = progressInfo.getPercent();
                mGlideProgress.setProgressValue(progress);
                mGlideProgressText.setText(progress + "%");
                LogUtils.log(LogUtils.DEBUG,TAG, "--Glide-- " + progress + " %  " + progressInfo.getSpeed() + " byte/s  " + progressInfo.toString());
                if (progressInfo.isFinish()) {
                    //说明已经加载完成
                }
            }

            @Override
            public void onError(long id, Exception e) {
                mHandler.postSyncTask(new Runnable() {
                    @Override
                    public void run() {
                        mGlideProgress.setProgressValue(0);
                        mGlideProgressText.setText("error");
                    }
                });
            }
        };
    }

```



#### 版本迭代
v1.0.0

#### 支持以下功能：
Glide 图片加载，文件下载和上传等进度显示。

#### 版权协议信息

```
 Copyright 2017, jessyan

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```















